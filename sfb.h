/*
Copyright (c) 2016 Ivane Gegia http://9221145.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <stdlib.h>
#include <math.h>
#include "saldefinitions.h"
#include "bmp.h"

#ifndef sfb_sfb_h
#define sfb_sfb_h

// Color base structure
typedef struct SFBColor
{
    float r;
    float g;
    float b;
} SFBColor;

typedef SFBColor* SFBColorRef;


// Frame buffer structure, holds actuall color buffer and other 
// state information
typedef struct SFBFramebuffer
{
    int width;
    int height;

    // Color with whic pixes are drawn
    SFBColor drawColor;

    // Color which which colorBuffer gets filled on clear operation
    SFBColor clearColor;

    SFBColor *colorBuffer;
    
} SFBFramebuffer;

typedef SFBFramebuffer* SFBFramebufferRef;

// Holds current drawing framebuffer
static SFBFramebufferRef currentFrameBuffer = NULL;

// Creates frame buffer object:
//
// PARAMETERS:
// width width of framebuffer in pixels
// height height of framebuffer in pixes
//
// RETURNS:
// Reference to SFBFrameBuffer or NULL if couldn't allocate memory
SFBFramebufferRef SFBFramebufferCreate(int width, int height)
{
    SFBFramebufferRef framebuffer = (SFBFramebufferRef)malloc(sizeof(SFBFramebuffer));
 
    if (framebuffer == NULL)
    {
        return NULL;
    }

    framebuffer->clearColor = (SFBColor){ 1.0F, 1.0F, 1.0F };
    framebuffer->drawColor  = (SFBColor){ 0.0F, 0.0F, 0.0F };

    framebuffer->width = width;
    framebuffer->height = height;

    framebuffer->colorBuffer = (SFBColorRef)malloc(width * height * sizeof(SFBColor));

    // Checking if color buffer was allocated properly
    // if not, free framebuffer and return NULL
    if (framebuffer->colorBuffer == NULL)
    {
        free(framebuffer);
        return NULL;
    }

    return framebuffer;
}

// Sets framebuffer as current framebuffer,
// meaning that all subsequent operations
// will happen in context of newly set
// current framebuffer.
//
// PARAMETERS:
// framebuffer framebuffer to set as current
void SFBFramebufferMakeCurrent(SFBFramebufferRef framebuffer)
{
    currentFrameBuffer = framebuffer;
}

// Sets drawing color for current framebuffer
void SFBSetDrawColor(SFBColor color)
{
    if (currentFrameBuffer == NULL)
    {
        return;
    }

    currentFrameBuffer->drawColor = color;
}

// Sets clear color for current framebuffer
void SFBSetClearColor(SFBColor color)
{
    if (currentFrameBuffer == NULL)
    {
        return;
    }

    currentFrameBuffer->clearColor = color;
}

// Clears current framebuffer's color buffer with clearColor
void SFBClear()
{
    if (currentFrameBuffer == NULL)
    {
        return;
    }

    int totalPixels = currentFrameBuffer->width * currentFrameBuffer->height;
    int pixelIndex = 0;

    for (pixelIndex = 0; pixelIndex < totalPixels; pixelIndex++)
    {
        currentFrameBuffer->colorBuffer[pixelIndex].r = currentFrameBuffer->clearColor.r;
        currentFrameBuffer->colorBuffer[pixelIndex].g = currentFrameBuffer->clearColor.g;
        currentFrameBuffer->colorBuffer[pixelIndex].b = currentFrameBuffer->clearColor.b;
    }

}

// Draw pixel in current framebuffer, where point { x = 0, y = 0 }
// is at upper left corner of framebuffer
// Thus interpreting frame buffer like:
//    (0,0)---------------+
//      |                 |      
//      |                 |      
//      |                 |      
//      |                 |      
//      +----------(width, height)
//
void SFBDrawPixel(int x, int y)
{
    if (currentFrameBuffer == NULL)
    {
        return;
    }

    if ((x < 0 || x > currentFrameBuffer->width) ||
        (y < 0 || y > currentFrameBuffer->height))
    {
        return;
    }

    int pixelIndex = y * currentFrameBuffer->width;
    pixelIndex = pixelIndex + x;

    currentFrameBuffer->colorBuffer[pixelIndex].r = currentFrameBuffer->drawColor.r;
    currentFrameBuffer->colorBuffer[pixelIndex].g = currentFrameBuffer->drawColor.g;
    currentFrameBuffer->colorBuffer[pixelIndex].b = currentFrameBuffer->drawColor.b;

}

// Draws point in framebuffer's color buffer
// interprets color buffer as space as:
//
// (-1.0,  1.0)-----------|----------- ( 1.0,  1.0)
//      |                 |                 |
//      |                 |                 |
//      |                 |                 |
//      |                 |                 |
//      |------------( 0.0,  0.0)-----------|
//      |                 |                 |
//      |                 |                 |
//      |                 |                 |
//      |                 |                 |
// (-1.0, -1.0)-----------|-----------( 1.0, -1.0)
//
void SFBDrawPoint(float x, float y)
{
    if (currentFrameBuffer == NULL)
    {
        return;
    }

    // If out of view then ignore point
    if ((x < -1.0 || x > 1.0F) ||
        (y < -1.0 || y > 1.0F))
    {
        return;
    }

    // Translate x and y values to range [0.0F...1.0F] 
    x = (x + 1.0F) / 2.0F;
    y = ( 2.0F - (y + 1.0F) ) / 2.0F;

    int xPixel = (int)floorf(x * currentFrameBuffer->width);
    int yPixel = (int)floorf(y * currentFrameBuffer->height);

    SFBDrawPixel(xPixel, yPixel);
}

void SFBFramebufferFree(SFBFramebufferRef framebuffer)
{
    free(framebuffer->colorBuffer);
    free(framebuffer);
}

// Writes contents of current framebuffer to BPM image file
//
// PARAMETERS:
// fileName
void SFBWriteToFileBMP(char *fileName)
{
    if (currentFrameBuffer == NULL)
    {
        return;
    }

    struct BMPBitmap *bmp;

    BMPBitmapCreate24Bit(currentFrameBuffer->width, currentFrameBuffer->height, &bmp);

    if (bmp != NULL)
    {
        int pixelCount = currentFrameBuffer->width * currentFrameBuffer->height;

        for (int pixelIndex = 0; pixelIndex < pixelCount; pixelIndex++)
        {
            int x = pixelIndex % currentFrameBuffer->width;
            int y = (int)floorf(pixelIndex / currentFrameBuffer->width);

            BMPBitmapWritePixelRGB24Bit(bmp, x, currentFrameBuffer->height - y,
                currentFrameBuffer->colorBuffer[pixelIndex].r,
                currentFrameBuffer->colorBuffer[pixelIndex].g,
                currentFrameBuffer->colorBuffer[pixelIndex].b);
        }

        BMPBitmapWriteToFile(bmp, fileName);

        BMPBitmapFree(bmp);
    }
}

#endif
