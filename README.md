#System independent frame buffer#

*Because not every log message is a text :) .*

Portable frame buffer, C library. For debugging purposes mainly.

*Copyright (c) 2016 Ivane Gegia http://9221145.com/*

**Programming language:** C

**Library prefix:** SFB

**Library type:** Header only

**License:** MIT (see [LICENSE.md](LICENSE.md))

**Main file:** sfb.h

#Usage sample#

```c
#include "sfb.h"


int main(void)
{
    SFBFramebufferRef fbRef = SFBFramebufferCreate(400, 300);

    SFBFramebufferMakeCurrent(fbRef);

    SFBSetDrawColor((SFBColor) { 1.0F, 0.0F, 0.0F });

    SFBSetClearColor((SFBColor) { 1.0F, 0.9F, 0.9F });

    SFBClear();

    const int PIXEL_COUNT = 100;

    for (int i_2 = 0; i_2 < PIXEL_COUNT; i_2++)
    {
        // Draws point in framebuffer's color buffer
        // interprets color buffer as space as:
        //
        // (-1.0,  1.0)-----------|----------- ( 1.0,  1.0)
        //      |                 |                 |
        //      |                 |                 |
        //      |                 |                 |
        //      |                 |                 |
        //      |------------( 0.0,  0.0)-----------|
        //      |                 |                 |
        //      |                 |                 |
        //      |                 |                 |
        //      |                 |                 |
        // (-1.0, -1.0)-----------|-----------( 1.0, -1.0)
        //
        SFBDrawPoint(i_2 / (float)PIXEL_COUNT, i_2 / (float)PIXEL_COUNT);
    }

    SFBWriteToFileBMP("fb1.bmp");
    
    SFBFramebufferFree(fbRef);

    return 0;
}
```

Which results in file like:

![fb1.bmp](fb1.bmp)