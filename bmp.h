/*
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Implementation follows specs from https://en.wikipedia.org/wiki/BMP_file_format

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#ifndef bmp_h
#define bmp_h

#define BPM_COMPRESSION__BI_RGB 0

#pragma pack(push, 1)
struct BMPBitmapHeader
{
    // "BM" c string
    uint8_t  BM[2];
    uint32_t BMPFileSize;
    uint16_t reserved1;
    uint16_t reserved2;

    //The offset, i.e.starting address, of the byte where the bitmap image data(pixel array) can be found
    uint32_t dataOffset;
};

struct BMPDIBHeaderCoreHeader
{
    // Header size (40 for DMPDIBBitmapInfoHeader)
    uint32_t headerSize;
    // Image width in pixels
    int32_t  imageWidthPixels;
    // Image height in pixels
    int32_t  imageHeightPixels;
    // The number of color planes (must be 1)
    uint16_t colorPlanes;
    // The number of bits per pixel (1,4,8, 16,24,32)
    uint16_t colorBitCount;
};

// https://msdn.microsoft.com/en-us/library/windows/desktop/dd183376(v=vs.85).aspx
struct BMPDIBBitmapInfoHeader
{
    // Extending CoreHeader
    struct BMPDIBHeaderCoreHeader coreHeader;
        
    // Set it to BPM_COMPRESSION__BI_RGB
    uint32_t compressionMethod;

    uint32_t imageSizeInBytes;

    // Horizontal resolutions in physical terms
    int32_t pixelsPerMeterWidth;

    // Vertical resolutions in physical terms
    int32_t pixelsPerMeterHeight;
    
    // 0, 2, 4, 8, 16, 32 ...
    uint32_t colorInColorPallete;

    // 0 is default
    uint32_t numberOfImportantColors;
};
#pragma pack(pop)

struct BMPBitmap
{
    struct BMPBitmapHeader bmpBitmapHeader;
    struct BMPDIBBitmapInfoHeader dibInfoHeader;
    int pixelBufferSize;
    void *pixelBuffer;
};

// Computes pixel buffer row size in bytes
uint32_t BMPComputeRowSize(uint16_t bitsPerPixel, int32_t imageWidthPixels)
{
    return (uint32_t)floorf(((bitsPerPixel * imageWidthPixels + 31) / 32.0F)) * 4;
}

// Computes pixel buffer size in bytes
uint32_t BMPComputePixelArraySize(uint16_t bitsPerPixel, int32_t imageWidthPixels, int32_t imageHeightPixels)
{
    return BMPComputeRowSize(bitsPerPixel, imageWidthPixels) * imageHeightPixels;
}

// Allocates pixel buffer for given BMP info header.
static void BMPAllocatePixelBuffer(_In_ const struct BMPDIBBitmapInfoHeader * bmpInfoHeader, _Outptr_result_maybenull_ void **pixelBuffer, _Out_ uint32_t *bufferSize)
{
    uint32_t pixelArraySize = BMPComputePixelArraySize(bmpInfoHeader->coreHeader.colorBitCount, bmpInfoHeader->coreHeader.imageWidthPixels, bmpInfoHeader->coreHeader.imageHeightPixels);

    void* pixelBufferPtr = malloc(pixelArraySize);

    for (unsigned int bIndex = 0; bIndex < pixelArraySize; bIndex++)
    {
        ((uint8_t*)pixelBufferPtr)[bIndex] = (uint8_t)0xff;
    }
    
    if (pixelBufferPtr == NULL)
    {
        *pixelBuffer = NULL;
        *bufferSize = 0;
        return;
    }

    *pixelBuffer = pixelBufferPtr;
    *bufferSize = pixelArraySize;

}

// Creates 24 bit bitmap
void BMPBitmapCreate24Bit(int32_t width, int32_t height, _Outptr_result_maybenull_ struct BMPBitmap **bmp)
{
    struct BMPBitmapHeader bitmapHeader;
    struct BMPDIBBitmapInfoHeader infoHeader;
    
    //Setting bitmap header
    bitmapHeader.BM[0] = 0x42;
    bitmapHeader.BM[1] = 0x4D;
    bitmapHeader.dataOffset = sizeof(struct BMPBitmapHeader) + sizeof(struct BMPDIBBitmapInfoHeader);
    bitmapHeader.reserved1 = 0;
    bitmapHeader.reserved2 = 0;
    
    //Setting core header
    infoHeader.coreHeader.imageWidthPixels = width;
    infoHeader.coreHeader.imageHeightPixels = height;
    infoHeader.coreHeader.colorPlanes = 1;
    infoHeader.coreHeader.headerSize = sizeof(struct BMPDIBBitmapInfoHeader);
    infoHeader.coreHeader.colorBitCount = 24;

    //Setting info header
    infoHeader.compressionMethod = BPM_COMPRESSION__BI_RGB;
    infoHeader.pixelsPerMeterWidth = 0;
    infoHeader.pixelsPerMeterHeight = 0;
    infoHeader.colorInColorPallete = 0;
    infoHeader.numberOfImportantColors = 0;
    
    void *pixelBuffer;

    //Allocating pixelBuffer
    BMPAllocatePixelBuffer(&infoHeader, &pixelBuffer, &(infoHeader.imageSizeInBytes));

    if (pixelBuffer == NULL)
    {
        *bmp = NULL;
        return;

    }

    //Allocating bitmap abstraction
    *bmp = (struct BMPBitmap*)malloc(sizeof(struct BMPBitmap));

    if (*bmp == NULL)
    {
        free(pixelBuffer);
        return;
    }

    //Copying required data, headers, pixelbuffer and other parameters in bitmap abstraction
    (*bmp)->bmpBitmapHeader = bitmapHeader;
    (*bmp)->dibInfoHeader = infoHeader;
    (*bmp)->pixelBuffer = pixelBuffer;
    (*bmp)->pixelBufferSize = infoHeader.imageSizeInBytes;

    (*bmp)->bmpBitmapHeader.BMPFileSize = infoHeader.coreHeader.headerSize + infoHeader.imageSizeInBytes;
}

void BMPBitmapWritePixelRGB24Bit(_In_ struct BMPBitmap *bitmap, int x, int y, float r, float g, float b)
{
    int rowSize = BMPComputeRowSize(bitmap->dibInfoHeader.coreHeader.colorBitCount, bitmap->dibInfoHeader.coreHeader.imageWidthPixels);

    int widthPx = bitmap->dibInfoHeader.coreHeader.imageWidthPixels;
    int heightPx = bitmap->dibInfoHeader.coreHeader.imageHeightPixels;
    
    if ((x < 0 || x > widthPx - 1) ||
        (y < 0 || y > heightPx - 1))
    {
        return;
    }

    r = r < 0.0F ? 0.0F : r;
    r = r > 1.0F ? 1.0F : r;

    g = g < 0.0F ? 0.0F : g;
    g = g > 1.0F ? 1.0F : g;

    b = b < 0.0F ? 0.0F : b;
    b = b > 1.0F ? 1.0F : b;

    int pixelIndex = rowSize  * y + x * 3;

    uint8_t *pixelBuffer = (uint8_t*)(bitmap->pixelBuffer);

    pixelBuffer[pixelIndex + 0] = (uint8_t)(b * 255);
    pixelBuffer[pixelIndex + 1] = (uint8_t)(g * 255);
    pixelBuffer[pixelIndex + 2] = (uint8_t)(r * 255);

}

void BMPBitmapWriteToFile(_In_ struct BMPBitmap *bitmap, _In_ char *fileName)
{
    FILE *file;

    file = fopen(fileName, "wb");

    if (file == NULL)
    {
        return;
    }

    // Writing bitmap header
    fwrite(&bitmap->bmpBitmapHeader.BM, sizeof(uint8_t), 2, file);
    fwrite(&bitmap->bmpBitmapHeader.BMPFileSize, sizeof(uint32_t), 1, file);
    fwrite(&bitmap->bmpBitmapHeader.reserved1, sizeof(uint16_t), 1, file);
    fwrite(&bitmap->bmpBitmapHeader.reserved2, sizeof(uint16_t), 1, file);
    fwrite(&bitmap->bmpBitmapHeader.dataOffset, sizeof(uint32_t), 1, file);

    // Writing core header
    fwrite(&bitmap->dibInfoHeader.coreHeader.headerSize, sizeof(uint32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.coreHeader.imageWidthPixels, sizeof(int32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.coreHeader.imageHeightPixels, sizeof(int32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.coreHeader.colorPlanes, sizeof(uint16_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.coreHeader.colorBitCount, sizeof(uint16_t), 1, file);

    // Writing DIB info header
    fwrite(&bitmap->dibInfoHeader.compressionMethod, sizeof(uint32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.imageSizeInBytes, sizeof(uint32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.pixelsPerMeterWidth, sizeof(int32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.pixelsPerMeterHeight, sizeof(int32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.colorInColorPallete, sizeof(uint32_t), 1, file);
    fwrite(&bitmap->dibInfoHeader.numberOfImportantColors, sizeof(uint32_t), 1, file);

    // Writing pixel buffer
    fwrite(bitmap->pixelBuffer, sizeof(uint8_t), bitmap->pixelBufferSize, file);
    

    if (file != NULL)
    {
        fclose(file);
    }
    
}

// Frees BMPBitmap and sub data associated with it
void BMPBitmapFree(_In_ struct BMPBitmap *bitmap)
{
    free(bitmap->pixelBuffer);
    free(bitmap);
}

#endif